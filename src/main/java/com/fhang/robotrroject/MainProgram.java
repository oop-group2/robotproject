/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.robotrroject;

import java.util.Scanner;

/**
 *
 * @author Natthakritta
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10,10);
        Robot robot = new Robot(2,2,'R',map);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {            
            map.showMap();
            //W == a , N == w , E == d , S==s ,q == quit
            char direction = inputDirection(kb);
            if (direction=='q') {
                System.out.println("Bye Bye!!!");
                break;
            }
            robot.walk(direction);
        }
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
